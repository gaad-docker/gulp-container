# Gulp container

Scss processing only for now

## Example config

```
COMPOSE_PROJECT_NAME=proj_name
PROJECT_DOCKER_DIR=docker_dir
ENV=dev
CONTAINER_USER=host_user_name
CONTAINER_UID=1000
CONTAINER_GID=1000
GULP_WORKDIR=a/b/c
GULP_TASK=watch
```
### Dev
```
    gulp:
    container_name: ${COMPOSE_PROJECT_NAME}_gulp
    build:
      context: ./${PROJECT_DOCKER_DIR}/gulp-container
      args: []
    restart: always
    environment:
      ENV: ${ENV}
      GULP_WORKDIR: ${GULP_WORKDIR}
      GULP_TASK: ${GULP_TASK}
      USER: ${CONTAINER_USER}
      GROUP: ${CONTAINER_USER}
      GID: ${CONTAINER_GID}
      UID: ${CONTAINER_UID}
    volumes:
      - ./:/app
    working_dir: /app
```

### Image
```
gaad/gulp:latest
```