FROM ubuntu:18.04

ENV ENV dev
ENV NODE_VERSION 16.x
ENV GULP_TASK default
ENV GULP_WORKDIR ""
ENV USER $USER
ENV GROUP ${GROUP:-$USER}
ENV GID ${GID}
ENV UID ${UID}

RUN apt-get update \
    && apt-get install -y \
    curl

RUN curl -sL "https://deb.nodesource.com/setup_$NODE_VERSION" | bash - \
    && apt-get install -y nodejs sudo \
    && npm install -g gulp package-json-merge

COPY ./gulpfile.js /tmp/gulpfile.js
COPY ./gulp-settings.js /tmp/gulp-settings.js
COPY ./package.json /tmp/package.json
COPY ./gulp-it.sh /docker-entrypoint.d/
COPY ./gulp-it-func.sh /tmp/

RUN chmod +x /docker-entrypoint.d/gulp-it.sh

ENTRYPOINT ["/docker-entrypoint.d/gulp-it.sh"]