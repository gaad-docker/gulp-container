const PATHS_DEV = {
  "styles": {
    "src": "./**/*.scss",
    "dest": "./css/"
  },
  "scripts": {
    "src": "./assets/js/**/*.js",
    "dest": "./js/"
  },
  "lang": {
    "src": "./languages/**/*.po",
    "dest": "./lang/"
  }
}

exports.PATHS = {
  dev: PATHS_DEV
}