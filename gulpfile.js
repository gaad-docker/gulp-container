const fs = require('fs');
const gulp = require('gulp');
const gutil = require("gulp-util");
const env = require("gulp-env");
const sass = require('gulp-sass')(require('sass'));
const {PATHS} = require('./gulp-settings');
const {fail} = require("assert");
const poToMo = require('gulp-potomo-js');

let ready = false;

function setEnv() {
    env({
        vars: {
            ENV: 'string' === typeof process.env.ENV ? process.env.ENV : 'dev'
        }
    });
}

function configTest() {
    let statusStack = [
        undefined !== PATHS[process.env.ENV]
    ];

    let statusTest = statusStack.filter(function (elem, pos) {
        return statusStack.indexOf(elem) === pos;
    });

    let status = statusTest.length === 1 && statusTest[0]
    ready = status;

    gutil.log('Config test: ' + (status ? 'ok' : 'error'));
}

gulp.task('styles', function () {

    let partName = 'styles';
    let src = PATHS[process.env.ENV][partName].src;
    let dest = PATHS[process.env.ENV][partName].dest;

    if (!ready) {
        let msg = 'Gulp is not ready for env: ' + process.env.ENV + '. Please check the gulp-settings.js file and add . const PATHS_' + process.env.ENV.toUpperCase() + ' object.';

        return gulp.src('.')
            .pipe(fail(msg));
    }

    return gulp.src(src)
        .pipe(sass())
        .pipe(gulp.dest(dest))
});

gulp.task('lang', function () {

        let partName = 'lang';
        let src = PATHS[process.env.ENV][partName].src;
        let dest = PATHS[process.env.ENV][partName].dest;

        return gulp.src(src)
            .pipe(poToMo())
            .pipe(gulp.dest(dest))
    }
);

gulp.task('watch', () => {
    gulp.watch(PATHS[process.env.ENV]['styles'].src, (done) => {
        gulp.series(['styles'])(done);
    });

    gulp.watch(PATHS[process.env.ENV]['lang'].src, (done) => {
        gulp.series(['lang'])(done);
    });
});



setEnv();
configTest();