#!/bin/bash
TAG_FILE=.dev/.tag
TAG=`cat ${TAG_FILE}`
VERSION=`echo ${TAG} | grep -Eo ':[\.0-9]*$' | grep -Eo '[\.0-9]*$'`
NAME=`cat ${TAG_FILE} | sed  s/:${VERSION}//g`

