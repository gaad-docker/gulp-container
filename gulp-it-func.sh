#!/bin/bash

set -x
set -e

#Host user mappings
function HostUserMap() {
  USER=$1
  UID=$2
  GID=$3

  if id "$USER" &>/dev/null; then
    echo "User $USER exists, proceeding."
  else
    echo "Creating user $USER."
    groupadd --gid "${GID}" -o "${USER}" &&
      useradd --gid "${GID}" --uid ${UID} --groups sudo --create-home "${USER}"
  fi
}

#Merge packages.json file with Gulp dependencies
function AddGulpDependenciesToPackageJson() {
  GULP_WORKDIR=$1
  USER=$2
  GROUP=$3

  if [[ ! -f "/tmp/${GULP_WORKDIR}/package-tmp.json" ]]; then
    package-json-merge /tmp/package.json "/app/${GULP_WORKDIR}/package.json" >"/tmp/package-tmp.json"
    cp /tmp/package-tmp.json "/app/${GULP_WORKDIR}/package.json"
    chown "$USER" "/app/${GULP_WORKDIR}/package.json"
    chgrp "$GROUP" "/app/${GULP_WORKDIR}/package.json"
  fi
}

# Gulpfile
function CreateGulpFile() {
  GULP_WORKDIR=$1
  USER=$2
  GROUP=$3

  if [[ ! -f "/app/${GULP_WORKDIR}/gulpfile.js" ]]; then
    TARGET=/app/${GULP_WORKDIR}/gulpfile.js
    cp /tmp/gulpfile.js "$TARGET"
    chmod 775 "$TARGET"
    chmod -x "$TARGET"
    chown -R "$USER" "$TARGET"
    chgrp -R "$GROUP" "$TARGET"
  else
    if [[ $ENV = "dev" ]]; then
      TARGET=/app/${GULP_WORKDIR}/gulpfile.js
      chmod -x "$TARGET"
      chown -R "$USER" "$TARGET"
      chgrp -R "$GROUP" "$TARGET"
    fi
  fi
}

# External Gulp settings file
function CreateExternalGulpSettingsFile() {
  GULP_WORKDIR=$1
  USER=$2
  GROUP=$3

  if [[ ! -f "/app/${GULP_WORKDIR}/gulp-settings.js" ]]; then
    TARGET=/app/${GULP_WORKDIR}/gulp-settings.js
    cp /tmp/gulp-settings.js "$TARGET"
    chmod 775 "$TARGET"
    chmod -x "$TARGET"
    chown "$USER" "${TARGET}"
    chgrp "$GROUP" "${TARGET}"
  else
    if [[ $ENV = "dev" ]]; then
      TARGET=/app/${GULP_WORKDIR}/gulp-settings.js
      chmod -x "$TARGET"
      chown "$USER" "${TARGET}"
      chgrp "$GROUP" "${TARGET}"
    fi
  fi
}

function NPMInstall(){
  GULP_WORKDIR=$1
  USER=$2
  GROUP=$3

  if [[ ! -d "/app/${GULP_WORKDIR}/node_modules" ]]; then
    cd /app/${GULP_WORKDIR}
    npm i

    if [[ $ENV = "dev" ]]; then
      chown -R "$USER" "/app/${GULP_WORKDIR}/node_modules"
      chgrp -R "$GROUP" "/app/${GULP_WORKDIR}/node_modules"

      chown -R "$USER" "/app/${GULP_WORKDIR}/package-lock.json"
      chgrp -R "$GROUP" "/app/${GULP_WORKDIR}/package-lock.json"
    fi
  fi
}

function RunGulp() {
  GULP_WORKDIR=$1
  ENV=$2
  GULP_TASK=$3

  gulp \
    "--cwd=/app/${GULP_WORKDIR}" \
    "--env=${ENV}" \
    -f "/app/${GULP_WORKDIR}/gulpfile.js" \
    "${GULP_TASK:-default}"
}