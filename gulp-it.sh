#! /bin/bash

set -x
set -e

source /tmp/gulp-it-func.sh

HostUserMap $USER $UID $GID
AddGulpDependenciesToPackageJson $GULP_WORKDIR $USER $GROUP
CreateGulpFile $GULP_WORKDIR $USER $GROUP
CreateExternalGulpSettingsFile $GULP_WORKDIR $USER $GROUP
NPMInstall $GULP_WORKDIR $USER $GROUP
RunGulp $GULP_WORKDIR $ENV $GULP_TASK